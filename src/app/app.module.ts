import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyApp } from './app.component';

import { TabsPage } from '../components/tabs/tabs';
import {GeneralComponent} from "../components/general/general";
import {ChatsComponent} from "../components/chats/chats";
import {ChatViewComponent} from "../share/components/chat-view/chat-view";
import { AuthProvider } from '../providers/auth/auth';
import {HttpModule} from "@angular/http";


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    GeneralComponent,
    ChatsComponent,
    ChatViewComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{tabsPlacement: 'top'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    GeneralComponent,
    ChatsComponent,
    ChatViewComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider
  ]
})
export class AppModule {}
