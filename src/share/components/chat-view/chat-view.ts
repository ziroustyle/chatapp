import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'chat-view',
  templateUrl: 'chat-view.html'
})
export class ChatViewComponent {

  @Input() chat:any;
  @Output() sendMessage = new EventEmitter();

  @ViewChild('myInput') myInput:any;


  constructor() {

  }

  outputMessage(){
    const text = this.myInput.nativeElement.innerText;

    if(text != '' && text != undefined) {
      this.sendMessage.emit(text);
    }

    this.myInput.nativeElement.innerText = '';
  }

}
