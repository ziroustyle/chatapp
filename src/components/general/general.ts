import { Component } from '@angular/core';
import {HOST} from '../../share/constans';
import * as io from 'socket.io-client'
import {AuthProvider} from "../../providers/auth/auth";

@Component({
  selector: 'general',
  templateUrl: 'general.html'
})
export class GeneralComponent {

  private socket:any;
  private chat:any = [];

  constructor(private auth: AuthProvider) {
    this.socket = io(HOST)

    this.socket.on('message', (msg) => {
      console.log("message", msg);
      this.chat.push(msg);
    });

    this.socket.on('configInitial',(msg) =>{
      console.log('config:');
      this.getInitialConfig(msg);
    });
  }



  sendMessage(msg){

    if(msg != '' && msg !== undefined){
      this.socket.emit('message', msg);
    }

  }

  getInitialConfig(msg){
    console.log(msg);
    this.auth.token = msg.id;
  }

}
