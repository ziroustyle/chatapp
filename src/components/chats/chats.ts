import { Component } from '@angular/core';

/**
 * Generated class for the ChatsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chats',
  templateUrl: 'chats.html'
})
export class ChatsComponent {

  text: string;

  constructor() {
    console.log('Hello ChatsComponent Component');
    this.text = 'Hello World';
  }

}
