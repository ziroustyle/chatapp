import {Component, ViewChild} from '@angular/core';

import {GeneralComponent} from "../general/general";
import {NavController, Tabs} from "ionic-angular";
import {ChatsComponent} from "../chats/chats";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;

  public badge1:any;
  public badge2:any;
  public badge3:any;

  tab1Root = GeneralComponent;
  tab2Root = ChatsComponent;
  tab3Root = GeneralComponent;

  constructor(public navCtrl: NavController) {

  }

  swipeEvent(event){
    if(event.direction == '2'){
      this.navCtrl.last();
      console.log("direction 2");
    }else if(event.direction == '4'){
      console.log("direction 4");
      console.log(this.navCtrl.getActive());
    }
  }

}
